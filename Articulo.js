class Articulo{
    constructor(nombre, precio, saldo){
        this.nombre = nombre;
        this.precio = precio;
        this.saldo = saldo;
    }
}

module.exports = Articulo;